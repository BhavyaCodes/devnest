const clickElement = document.querySelector(".click");
const buttonElement = document.querySelector(".button");
const draggableElement = document.querySelector(".draggable");
const inputElement = document.querySelector("#input");
const formElement = document.querySelector(".form");

clickElement.addEventListener("click", function (e) {
  console.log("clicked me");
});

buttonElement.addEventListener("focus", function (e) {
  console.log("focussing button");
});

buttonElement.addEventListener("blur", function (e) {
  console.log("unfocussing button");
});

draggableElement.addEventListener("drag", function (e) {
  console.log(e);
});

inputElement.addEventListener("change", function (e) {
  console.log(e.target.value);
});

formElement.addEventListener("submit", function (e) {
  e.preventDefault();
  console.log(e);
});

window.addEventListener("resize", function (e) {
  console.log("resizing window");
});

window.addEventListener("keydown", function (e) {
  console.log(e);
});

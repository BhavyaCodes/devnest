const minuteHand = document.querySelector(".minute");
const secondHand = document.querySelector(".second");
const hourHand = document.querySelector(".hour");

setInterval(() => {
  const date = new Date();
  const minutes = date.getMinutes();
  const minutesDeg = (minutes / 60) * 360 - 90;
  minuteHand.style["transform"] = `translate(0, -50%) rotate(${minutesDeg}deg)`;
  const seconds = date.getSeconds();
  const secondsDeg = (seconds / 60) * 360 - 90;
  secondHand.style["transform"] = `translate(0, -50%) rotate(${secondsDeg}deg)`;
  const hours = date.getHours();
  const hoursDef = (hours / 12) * 360 - 90;
  hourHand.style["transform"] = `translate(0, -50%) rotate(${hoursDef}deg)`;
}, 1000);

import "./Card.css";

function Card({ imageUrl, text }) {
  return (
    <div className="card">
      <img className="card__image" src={imageUrl} alt="card-text" />
      <p>{text}</p>
    </div>
  );
}

export default Card;

import "./App.css";
import Card from "./components/Card";

function App() {
  return (
    <div className="App">
      <Card
        imageUrl="https://m.media-amazon.com/images/M/MV5BMDYzYmQ4MGEtMTM3MS00NWNlLTk2YmMtNTYzMmE2N2YyYWY1XkEyXkFqcGdeQXVyODQwMDcwNDY@._V1_.jpg"
        text="Chattan Singh"
      />
    </div>
  );
}

export default App;

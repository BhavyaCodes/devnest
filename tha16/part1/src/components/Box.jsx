import "./Box.css";

function Box({ color }) {
  return <div style={{ backgroundColor: color }} className="box" />;
}

export default Box;

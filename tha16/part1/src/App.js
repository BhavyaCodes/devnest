import "./App.css";
import Box from "./components/Box";
function App() {
  const color1 = "black";
  const color2 = "lime";

  const renderBoxes = () => {
    const boxes = [];
    for (let i = 0; i < 64; i++) {
      boxes.push(
        <Box color={(i + Math.floor(i / 8)) % 2 === 0 ? color1 : color2} />
      );
    }
    return boxes;
  };

  return <div className="App">{renderBoxes()}</div>;
}

export default App;

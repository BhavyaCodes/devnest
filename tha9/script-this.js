const student1 = {
  name: "Chattan Singh",
  greet(greeting, title) {
    console.log(greeting + title + this.name);
  },
};

const student2 = {
  name: "daaku",
};

student1.greet.call(student2, "Hello ", "Mr ");
student1.greet.apply(student2, ["Hello ", "Mr "]);

const bindedFunction = student1.greet.bind(student2, "Good morning ", "Mr ");
bindedFunction();

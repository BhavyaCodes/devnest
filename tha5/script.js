const input1Element = document.querySelector("#number-1");
const input2Element = document.querySelector("#number-2");

const addBtn = document.querySelector(".btn-add");
const subtractBtn = document.querySelector(".btn-subtract");
const multiplyBtn = document.querySelector(".btn-multiply");
const divideBtn = document.querySelector(".btn-divide");

const answerDiv = document.querySelector(".answer");

function calculateAndDisplay(cb) {
  const answer = cb(+input1Element.value, +input2Element.value);
  answerDiv.innerText = answer;
}

addBtn.addEventListener(
  "click",
  calculateAndDisplay.bind(this, (a, b) => a + b)
);

subtractBtn.addEventListener(
  "click",
  calculateAndDisplay.bind(this, (a, b) => a - b)
);

multiplyBtn.addEventListener(
  "click",
  calculateAndDisplay.bind(this, (a, b) => a * b)
);

divideBtn.addEventListener(
  "click",
  calculateAndDisplay.bind(this, (a, b) => a / b)
);

// addBtn.addEventListener("click", function () {
//   const number1 = Number(input1Element.value);
//   const number2 = Number(input2Element.value);
//   const sum = number1 + number2;
//   answerDiv.innerText = sum;
// });

// subtractBtn.addEventListener("click", function () {
//   const number1 = Number(input1Element.value);
//   const number2 = Number(input2Element.value);
//   const answer = number1 - number2;
//   answerDiv.innerText = answer;
// });

// multiplyBtn.addEventListener("click", function () {
//   const number1 = Number(input1Element.value);
//   const number2 = Number(input2Element.value);
//   const answer = number1 * number2;
//   answerDiv.innerText = answer;
// });

// divideBtn.addEventListener("click", function () {
//   const number1 = Number(input1Element.value);
//   const number2 = Number(input2Element.value);
//   const answer = number1 / number2;
//   answerDiv.innerText = answer;
// });

const defaultTodos = [
  { checked: false, text: "finish THA-12" },
  { checked: true, text: "be awesome" },
];

//get initial todos
let list;
const listFromLocalStorage = window.localStorage.getItem("devnest-todos");
if (listFromLocalStorage) {
  list = JSON.parse(listFromLocalStorage);
} else {
  list = [...defaultTodos];
}

console.log(list);

function saveToLocalStorage(list) {
  window.localStorage.setItem("devnest-todos", JSON.stringify(list));
}

//print todos

function printTodos() {
  const ulElement = document.querySelector("ul");
  ulElement.innerHTML = "";
  list.forEach((todo, i) => {
    const liElement = document.createElement("li");
    const label = document.createElement("label");
    label.innerText = todo.text;
    const checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    if (todo.checked) {
      checkbox.setAttribute("checked", true);
      label.classList.add("checked");
    }
    checkbox.addEventListener("change", function (e) {
      list[i].checked = !list[i].checked;
      saveToLocalStorage(list);
      printTodos();
    });
    const deleteButton = document.createElement("button");
    deleteButton.innerText = "Delete";
    deleteButton.addEventListener("click", function (e) {
      list.splice(i, 1);
      saveToLocalStorage(list);
      printTodos();
    });
    liElement.appendChild(checkbox);
    liElement.appendChild(label);
    liElement.appendChild(deleteButton);
    ulElement.appendChild(liElement);
  });
}

printTodos();

//add new todo

const form = document.querySelector("form");
const inputElement = document.querySelector("#input");
form.addEventListener("submit", (e) => {
  e.preventDefault();

  console.log(inputElement.value);
  list.push({
    checked: false,
    text: inputElement.value,
  });
  inputElement.value = "";
  saveToLocalStorage(list);
  printTodos();
});
